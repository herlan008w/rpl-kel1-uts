Feature: Checking if a number is prime

    Scenario: Checking if a number is prime
        Given I have entered the number 7
        When I check if it is a prime number
        Then it should say it's a prime number

    Scenario: Checking if a number is not prime
        Given I have entered the number 4
        When I check if it is a prime number
        Then it should say it's not a prime number

    Scenario: Checking if a number
        Given I have entered the number A
        When I check if it is not number
        Then it should say it's not a number 

    Scenario: Checking if a number is not prime
        Given I have entered the number 8
        When I check if it is a prime number
        Then it should say it's not a prime number
