// features/step_definitions/steps.js
const { Given, When, Then } = require('cucumber');
const { isPrime, isNumber } = require('../../prime');

let number;
let result;

Given('I have entered the number {int}', function (num) {
    number = num;
});

When('I check if it is a prime number', function () {
    result = isPrime(number);
});

Then('it should say it\'s a prime number', function () {
    expect(result).toBe(true);
});

Then('it should say it\'s not a prime number', function () {
    expect(result).toBe(false);
});

Given('I have entered the value {string}', function (value) {
    number = parseFloat(value);
});

Then('it should say it is a number', function () {
    expect(isNumber(number)).toBe(true);
});
