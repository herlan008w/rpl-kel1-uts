// tests/prime.test.js
const { isPrime, isNumber } = require('../prime');

test('7 is a prime number', () => {
    expect(isPrime(7)).toBe(true);
});

test('4 is not a prime number', () => {
    expect(isPrime(4)).toBe(false);
});

test('A is not a number', () => {
    expect(isPrime(A)).toBe(false);
});

test('8 is not a prime number', () => {
    expect(isPrime(8)).toBe(false);
});
