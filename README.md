# Automate Testing: Mengecek Bilangan Prima

Proyek ini bertujuan untuk melakukan automate testing pada fungsi-fungsi yang digunakan untuk mengecek apakah sebuah nilai adalah bilangan prima atau bukan. Proyek ini menggunakan Jest JS, Cucumber, dan Gherkin style dengan Node.js.

## Instalasi

1. Pastikan Anda memiliki Node.js terinstal di komputer Anda. Jika belum, Anda dapat mengunduhnya dari [situs web resmi Node.js](https://nodejs.org/).
2. Unduh atau clone repository ini ke dalam komputer Anda.
3. Buka terminal dan arahkan ke direktori proyek ini.
4. Jalankan perintah `npm install` untuk menginstal semua dependensi yang diperlukan.

## Struktur Proyek

kelompok-2/
  ├── features/
  │   └── prime.feature
  ├── step_definitions/
  │   └── steps.js
  ├── tests/
  │   └── prime.test.js
  ├── package.json
  └── prime.js

## Cara Penggunaan

1. **Mengecek Bilangan Prima:**
   - Fungsi untuk mengecek apakah sebuah nilai adalah bilangan prima atau bukan terdapat dalam file `prime.js`.
   - Skenario pengujian menggunakan Cucumber dituliskan dalam file `prime.feature`.
   - Implementasi step definitions untuk skenario pengujian ada di dalam file `steps.js`.

2. **Menjalankan Pengujian:**
   - Untuk menjalankan pengujian menggunakan Jest, jalankan perintah `npx jest` di terminal.

## Kontribusi
Jika Anda ingin berkontribusi pada proyek ini, silakan buka pull request atau laporkan masalah melalui fitur Issues.

## Lisensi
Proyek ini dilisensikan di bawah lisensi [MIT](LICENSE).
