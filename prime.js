// prime.js

function isPrime(num) {
    // Memeriksa apakah input adalah angka
    if (!isNumber(num)) {
        throw new Error('Input harus berupa angka!');
    }
    // Memeriksa apakah input adalah bilangan prima
    if (num <= 1) return false;
    if (num === 2) return true;
    for (let i = 2; i <= Math.sqrt(num); i++) {
        if (num % i === 0) return false;
    }
    return true;
}

function isNumber(value) {
    return !isNaN(value) && typeof value === 'number';
}

module.exports = { isPrime, isNumber };
